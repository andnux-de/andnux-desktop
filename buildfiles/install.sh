#!/bin/sh
apt-get update && apt-get install kwin tint2 feh sddm
mkdir "/usr/share/andnux"
wget -O "http://andnux.rf.gd/wp-content/uploads/2020/01/2020-landscape-1-1.png" "/usr/share/andnux/backdrop.png"
/bin/cat <<EOF >"/usr/share/andnux/launch.sh"
#!/bin/sh
tint2 &
feh --bg-fill "/usr/share/andnux/backdrop.png" &
kwin
EOF
/bin/cat <<EOF >"/usr/share/xsessions/andnux.desktop"
[Desktop Entry]
Name=Andnux Desktop
Comment=When the world is the breath...
Exec=/usr/share/andnux/launch.sh
TryExec=/usr/share/andnux/launch.sh
Type=XSession
EOF
chmod +x "/usr/share/andnux/launch.sh"
